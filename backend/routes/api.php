<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ResetPasswordController;
use Illuminate\Support\Facades\Route;


Route::post('/register', [AuthController::class, 'registerUser'])->name('auth.register');
Route::post('/register-deliverer', [AuthController::class, 'registerDeliverer'])->name('auth.registerDeliverer');

//rota de login para usuário comum
Route::post('/login-client', [AuthController::class, 'loginClient'])->name('auth.loginClient');

//rota de login para usuário entregador
Route::post('/login-deliverer', [AuthController::class, 'loginDeliverer'])->name('auth.loginDeliverer');

//rota para enviar email de recuperação, o usuário insere o email dele
Route::post('/forget-client', [ResetPasswordController::class, 'sendEmail'])->name('reset.sendEmail');
Route::post('/forget-deliverer', [ResetPasswordController::class, 'sendEmailDeliverer'])->name('reset.sendEmailDeliverer');

//quando o usuário clicar no link, o front redireciona para rota de reset-client ou reset-deliverer
Route::get('/{email}/{token}', [ResetPasswordController::class, 'authCredentials'])->name('authCredentials');



Route::middleware('auth:sanctum')->group(function () {
    //Somente os usuários logados acessam essas rotas

    //pagina inicial do cliente - tem os dados do cliente e as entregas que ele cadastrou
    Route::get('/client-dashboard', [DashboardController::class, 'clientDashboard'])->name('dashboard.client');

    //pagina inicial do entregador - tem os dados dele, as entregas disponíveis, etc
    Route::get('/deliverer-dashboard', [DashboardController::class, 'delivererDashboard'])->name('dashboard.deliverer');

    //cria uma nova entrega
    Route::post('/new-delivery', [DashboardController::class, 'newDelivery'])->name('dashboard.newDelivery');

    //confirma entrega
    //o entregador passa o cpf de que pediu e o name_receiver (nome de quem vai receber)
    Route::post('/confirm/{delivery}', [DashboardController::class, 'confirm'])->name('dashboard.confirm');

    //editar perfil do cliente
    Route::post('/edit-client', [DashboardController::class, 'editClient'])->name('dashboard.editClient');
    //editar perfil do entregador
    Route::post('/edit-deliverer', [DashboardController::class, 'editDeliverer'])->name('dashboard.editDeliverer');

    //rota para alterar a senha do usuário
    Route::post('/reset-client', [AuthController::class, 'resetClient'])->name('auth.resetClient');
    Route::post('/reset-deliverer', [AuthController::class, 'resetDeliverer'])->name('auth.resetDeliverer');

    //logout do usuário comum, passando o id do usuário
    Route::post('/logout-client/{user}', [AuthController::class, 'logoutClient'])->name('auth.logoutClient');

    //logout do entregador, passando o id do entregador
    Route::post('/logout-deliverer/{deliverer}', [AuthController::class, 'logoutDeliverer'])->name('auth.logoutDeliverer');
});
