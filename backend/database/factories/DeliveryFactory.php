<?php

namespace Database\Factories;

use App\Models\Delivery;
use App\Models\Vehicle;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeliveryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Delivery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function withFaker()
    {
        return \Faker\Factory::create('pt_BR');
    }
    public function definition()
    {
        return [
            'product' => $this->faker->name(),
            'delivery_place' => $this->faker->name(),
            'pickup_place' => Vehicle::factory(),
            'receive_client' => User::factory(),
            'user_id' => User::factory()
        ];
    }
}
