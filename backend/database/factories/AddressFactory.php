<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Deliverer;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'street'=> $this->faker->address(),
            'neighborhood'=> $this->faker->name(),
            'number'=> $this->faker->numberBetween(0,1),
            'city'=> $this->faker->name(),	
            'state'=> $this->faker->name(),
            'deliverer_id'=> Deliverer::factory(),
        ];
    }
}
