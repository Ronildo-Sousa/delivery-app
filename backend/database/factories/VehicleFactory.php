<?php

namespace Database\Factories;

use App\Models\Deliverer;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vehicle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function withFaker()
    {
        return \Faker\Factory::create('pt_BR');
    }
    public function definition()
    {
        return [
            'plaque' => $this->faker->unique()->numberBetween(0,100),
            'color' => $this->faker->colorName(),
            'model' => $this->faker->name(),
            'document' => $this->faker->numberBetween(0,5),
            'deliverer_id' => Deliverer::factory()
        ];
    }
}
