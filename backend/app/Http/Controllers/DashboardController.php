<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmRequest;
use App\Http\Requests\DeliveryRequest;
use App\Models\Deliverer;
use App\Models\Delivery;
use App\Models\Signature;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    public function clientDashboard()
    {
        $userData = User::find(Auth::user()->id)->with('address')->get();
        $doneDeliveries = Delivery::where('user_id', Auth::user()->id)->where('status', 1)->get();
        $pendingDeliveries = Delivery::where('user_id', Auth::user()->id)->where('status', 0)->where('deliverer_id', '!=', null)->get();
        $openDeliveries = Delivery::where('user_id', Auth::user()->id)->where('status', 0)->where('deliverer_id', null)->get();

        return response()->json([
            'user' => $userData,
            'done_deliveries' => $doneDeliveries,
            'pending_deliveries' => $pendingDeliveries,
            'open_deliveries' => $openDeliveries
        ], Response::HTTP_OK);
    }

    public function delivererDashboard()
    {
        $userData = Deliverer::find(Auth::user()->id)->with(['address', 'vehicles'])->get();
        $deliveries = Delivery::where('deliverer_id', null)->get();
        $pendingDeliveries = Delivery::where('status', 0)->where('deliverer_id', Auth::user()->id)->get();
        $doneDeliveries = Delivery::where('status', 1)->where('deliverer_id', Auth::user()->id)->get();

        return response()->json([
            'user' => $userData,
            'available_deliveries' => $deliveries,
            'pending_deliveries' => $pendingDeliveries,
            'done_deliveries' => $doneDeliveries
        ], Response::HTTP_OK);
    }

    public function newDelivery(DeliveryRequest $request)
    {
        $newDelivery = Delivery::create([
            'product' => $request->product,
            'delivery_place' => $request->delivery_place,
            'pickup_place' => $request->pickup_place,
            'receive_client' => $request->receive_client,
            'user_id' => Auth::user()->id
        ]);

        return response()->json([
            'newDelivery' => $newDelivery
        ], Response::HTTP_CREATED);
    }

    public function confirm(ConfirmRequest $request, Delivery $delivery)
    {
        Signature::create([
            'cpf_receiver' => $request->cpf_receiver,
            'name_receiver' => $request->name_receiver,
            'delivery_id' => $delivery->id
        ]);

        $delivery->status = true;
        $delivery->save();

        return response()->json(['message' => 'Entrega confirmada com sucesso', 'delivery' => $delivery], Response::HTTP_CREATED);
    }

    public function editClient(Request $request)
    {
        if ($request->phone) {
            $request->validate(['phone' => 'celular_com_ddd']);
        }
        $user = User::where('id', Auth::user()->id)->with(['address'])->first();
        strlen($request->name) ? ($user->name = $request->name) : "";
        strlen($request->password) ? ($user->password = bcrypt($request->password)) : "";
        strlen($request->phone) ? ($user->phone = $request->phone) : "";
        strlen($request->image) ? ($user->image = $request->image) : "";
        strlen($request->street) ? ($user->address->street = $request->street) : "";
        strlen($request->neighborhood) ? ($user->address->neighborhood = $request->neighborhood) : "";
        strlen($request->number) ? ($user->address->number = $request->number) : "";
        strlen($request->city) ? ($user->address->city = $request->city) : "";
        strlen($request->state) ? ($user->address->state = $request->state) : "";
        $user->save();
        $user->address->save();

        return response()->json([
            'message' => "Dados alterados com sucesso.",
            'user' => $user
        ]);
    }

    public function editDeliverer(Request $request)
    {
        if (($request->phone && $request->plaque)) {
            $request->validate(['phone' => 'celular_com_ddd', 'plaque' => 'formato_placa_de_veiculo']);
        }
        if ($request->plaque) {
            $request->validate(['plaque' => 'formato_placa_de_veiculo']);
        }
        if ($request->phone) {
            $request->validate(['phone' => 'celular_com_ddd']);
        }
        $deliverer = Deliverer::where('id', Auth::user()->id)->with(['address', 'vehicles'])->first();
        strlen($request->name) ? ($deliverer->name = $request->name) : "";
        strlen($request->password) ? ($deliverer->password = bcrypt($request->password)) : "";
        strlen($request->phone) ? ($deliverer->phone = $request->phone) : "";
        strlen($request->image) ? ($deliverer->image = $request->image) : "";
        strlen($request->cnh_image) ? ($deliverer->cnh_image = $request->cnh_image) : "";
        strlen($request->street) ? ($deliverer->address->street = $request->street) : "";
        strlen($request->neighborhood) ? ($deliverer->address->neighborhood = $request->neighborhood) : "";
        strlen($request->number) ? ($deliverer->address->number = $request->number) : "";
        strlen($request->city) ? ($deliverer->address->city = $request->city) : "";
        strlen($request->state) ? ($deliverer->address->state = $request->state) : "";
        strlen($request->model) ? ($deliverer->vehicles->model = $request->model) : "";
        strlen($request->plaque) ? ($deliverer->vehicles->plaque = $request->plaque) : "";
        strlen($request->color) ? ($deliverer->vehicles->color = $request->color) : "";
        strlen($request->document) ? ($deliverer->vehicles->document = $request->document) : "";
        $deliverer->save();
        $deliverer->address->save();
        $deliverer->vehicles->save();

        return response()->json([
            'message' => "Dados alterados com sucesso.",
            'deliverer' => $deliverer
        ]);
    }
}
