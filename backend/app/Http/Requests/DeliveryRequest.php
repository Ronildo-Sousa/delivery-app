<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product' => 'required|min:5',
            'delivery_place' => 'required|min:10',
            'pickup_place' =>'required|min:10',
            'receive_client' =>'required|min:4'
        ];
    }

    public function messages()
    {
        return [
            'product.required' => 'Campo produto é obrigatório.',
            'delivery_place.required' => 'Campo lugar de entrega é obrigatório.',
            'pickup_place.required' => 'Campo lugar de retirada é obrigatório.',
            'receive_client.required' => 'Campo cliente que receberá é obrigatório.',
            'product.min' => 'Produto deve ter no mínimo 5 caracteres.',
            'delivery_place.min' => 'Lugar de entrega deve ter no mínimo 10 caracteres.',
            'pickup_place.min' => 'Lugar de retirada deve ter no mínimo 10 caracteres.',
            'receive_client.min' => 'Cliente que receberá deve ter no mínimo 10 caracteres.'
        ];
    }
}
