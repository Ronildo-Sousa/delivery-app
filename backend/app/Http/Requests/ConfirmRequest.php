<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf_receiver' => 'required',
            'name_receiver' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cpf.required' => 'Campo cpf é obrigatório',
            'name_receiver.required' => 'Campo nome do recebedor é obrigatório'
        ];
    }
}
