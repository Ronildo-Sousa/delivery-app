<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'image' => 'required',
            'cpf' => 'required|cpf|unique:users,cpf',
            'phone' => 'required|celular_com_ddd',
            'street' => 'required',
            'neighborhood' => 'required',
            'number' =>'required',
            'city' =>'required',
            'state' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo nome é obrigatório.',
            'email.required' => 'Campo email é obrigatório.',
            'image.required' => 'Campo imagem é obrigatório.',
            'phone.required' => 'Campo telefone é obrigatório.',
            'phone.celular_com_ddd' => 'Telefone inválido.',
            'street.required' => 'Campo rua é obrigatório.',
            'neighborhood.required' => 'Campo bairro é obrigatório.',
            'number.required' => 'Campo numero é obrigatório.',
            'city.required' => 'Campo cidade é obrigatório.',
            'state.required' => 'Campo estado é obrigatório.',
            'cpf.required' => 'Campo cpf é obrigatório.',
            'cpf.cpf' => 'Insira um cpf válido.',
            'cpf.cpf' => 'Cpf indisponível.',
            'email.unique' => 'Email indisponível.',
            'email.email' => 'Insira um email válido.',
            'password.required' => 'Campo senha é obrigatório.',
            'password.confirmed' => 'As senhas são diferentes.'
        ];
    }

}
